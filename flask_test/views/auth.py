from flask import request, render_template, session, url_for, redirect, Blueprint
from sqlalchemy.exc import MultipleResultsFound, NoResultFound

from flask_test.db import db
from flask_test.models import UserModel

auth_route = Blueprint('auth', __name__, url_prefix='/user')

@auth_route.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':

        user_name = request.form['username']
        password = request.form['password']

        try:
            user = db.session.query(UserModel.user_name.label('login'),
                                    UserModel.name).filter(
                UserModel.user_name == user_name
            ).filter(
                UserModel.password == password
            ).one()
            # f = UserModel.query.filter_by(user_name=user_name, password=password).all()
            '''
            select user.user_name as login, user.name from user where user.user_name = ? and user.password = ?
            '''
        except MultipleResultsFound:
            return render_template('error.html', message='Знайдено більше одного користувача.')
        except NoResultFound:
            return render_template('error.html', message='Такого користувача не знайдено')

        session.clear()
        session['user'] = {'user_name': user.login, 'name': user.name}
        return redirect(url_for('auth.profile'))
    user = session.get('user')
    if user is not None:
        return redirect(url_for('auth.profile'))
    return render_template('register.html')

@auth_route.route('/profile')
def profile():
    user = session.get('user')
    if user is not None:
        return render_template('user.html', login=user['user_name'], name=user['name'])
    return redirect(url_for('auth.login'))

@auth_route.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        user_name = request.form['username']
        password = request.form['password']
        user = UserModel(user_name=user_name, password=password)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('auth.profile'))
    return render_template('register.html')

@auth_route.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('auth.profile'))