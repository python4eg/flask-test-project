from flask import Blueprint

base_route = Blueprint('base', __name__)

@base_route.route('/')
def root():
    return 'Hello World'


@base_route.route('/<file_name>')
def open_file(file_name):
    try:
       f = open(file_name)
       return f.read().replace('\n', '</br>')
    except FileNotFoundError:
        return '<h1>File not found</h1>'
