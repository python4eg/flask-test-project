from flask import Flask
from flask_test.config import FlaskConfig
from flask_test.db import db


def create_app():
    app = Flask(__name__)
    app.config.from_object(FlaskConfig)
    db.init_app(app)

    from flask_test.views import base
    from flask_test.views import auth

    app.register_blueprint(base.base_route)
    app.register_blueprint(auth.auth_route)
    return app


app = create_app()