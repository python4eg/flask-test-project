from db import db
from flask_test import app


if __name__ == '__main__':
    db.create_all(app=app)
    app.run()
