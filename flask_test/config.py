class FlaskConfig:
    DEBUG = True
    SECRET_KEY = 'some_random_key'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///new.db?uri=true'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
