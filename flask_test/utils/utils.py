import csv
import requests
from typing import Dict, List

astros_endpoint = 'http://api.open-notify.org/astros.json'

def get_formatted_astros(data: Dict[str, List]):
    """
    :param data: {'people': [
       {'craft': 'ISS', 'name': 'Astro1'},
       {'craft': 'Tan', 'name': 'Astro2'},
    ]}
    :return: {
    'ISS': [{'craft': 'ISS', 'name': 'Astro1'}]
    'Tan': [{'craft': 'Tan', 'name': 'Astro2'}]
    }
    """
    result = {}
    ppl = data.get('people', [])
    for i in ppl:
        if i['craft'] not in result:
            result[i['craft']] = []
        result[i['craft']].append(i)
    return result


def save(data: Dict[str, List]):
    with open('../ppl.csv', 'w+', newline='') as f:
        all_data = list(data.values())
        writer = csv.DictWriter(f, fieldnames=['craft', 'name'])
        writer.writeheader()
        for craft_data in all_data:
            writer.writerows(craft_data)
    for craft in data:
        with open(f'../{craft}.csv', 'w+', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=['craft', 'name'])
            writer.writeheader()
            writer.writerows(data[craft])


def get_and_save():
    data = requests.get(astros_endpoint)
    data_json = data.json()
    formatted_data = get_formatted_astros(data_json)
    save(formatted_data)


if __name__ == '__main__':
    get_and_save()