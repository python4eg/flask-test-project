import pytest

from flask_test.flask_test import create_app


@pytest.fixture
def fixture_1():
    print('Called fixture 1')
    return 'Some data'

@pytest.fixture(scope='session')
def fixture_2():
    print('Called fixture 2')
    return 'Some data 2'


@pytest.fixture
def client():
    app = create_app()
    with app.test_client() as client:
        yield client

