from unittest.mock import patch, MagicMock, call, Mock

import pytest

from flask_test.utils.utils import get_formatted_astros, save, get_and_save


def test_formatted_data_full_data(fixture_1):
    print(f'test_formatted_data_full_data called {fixture_1}')
    expected_data = {
        'test_craft': [{'craft': 'test_craft', 'name': 'test_name'}]
    }
    fake_input = {
        'people': [
            {'craft': 'test_craft', 'name': 'test_name'}
        ]
    }
    actual_data = get_formatted_astros(fake_input)
    assert actual_data == expected_data


def test_formatted_data__correct_keys(fixture_1):
    print(f'test_formatted_data__correct_keys called {fixture_1}')
    expected_data = ['test_craft']
    fake_input = {
        'people': [
            {'craft': 'test_craft', 'name': 'test_name'}
        ]
    }
    actual_data = get_formatted_astros(fake_input)
    assert list(actual_data) == expected_data


def test_formatted_data__bad_input(fixture_2):
    print(f'test_formatted_data__bad_input called {fixture_2}')
    expected_data = {}
    fake_input = {
        'astros': [
            {'craft': 'test_craft', 'name': 'test_name'}
        ]
    }
    actual_data = get_formatted_astros(fake_input)
    assert actual_data == expected_data


@pytest.mark.parametrize('expected_data,fake_input', [
    ({
         'test_craft': [{'craft': 'test_craft', 'name': 'test_name'}]
     },
     {
         'people': [
             {'craft': 'test_craft', 'name': 'test_name'}
         ]
     }),
    ({},
     {
         'astros': [
             {'craft': 'test_craft', 'name': 'test_name'}
         ]
     }
     )
])
def test_different_cases(expected_data, fake_input, fixture_2):
    print(f'test_different_cases called {fixture_2}')
    actual_data = get_formatted_astros(fake_input)
    assert actual_data == expected_data


@pytest.mark.skip('Неробочий тест. Так треба. Доданих для демонстрації')
@pytest.mark.parametrize('expected_data', [
    {
        'test_craft': [{'craft': 'test_craft', 'name': 'test_name'}]
    },
    {}
])
@pytest.mark.parametrize('fake_input', [
    {
        'people': [
            {'craft': 'test_craft', 'name': 'test_name'}
        ]
    },
    {
        'astros': [
            {'craft': 'test_craft', 'name': 'test_name'}
        ]
    }
])
def test_different_cases_option_2(expected_data, fake_input):
    actual_data = get_formatted_astros(fake_input)
    assert actual_data == expected_data


def test_save():
    fake_input = {
        'test_craft': [{'craft': 'test_craft', 'name': 'test_name'}]
    }
    with patch('builtins.open', MagicMock()) as mock:
        save(fake_input)
        mock.assert_called()
        mock.assert_called_with('../test_craft.csv', 'w+', newline='')
        mock.assert_has_calls(calls=[call('../ppl.csv', 'w+', newline='')])

def test_full_journey():
    fake_resp = {
            'people': [
                {'craft': 'test_craft', 'name': 'test_name'}
            ]
        }
    with patch('requests.get', Mock(name='mock_get')) as req_mock, patch('builtins.open', MagicMock()) as open_mock:
        mock_resp = Mock(name='mock_resp')
        mock_resp.json.return_value = fake_resp
        req_mock.return_value = mock_resp
        get_and_save()
        open_mock.assert_called_with('../test_craft.csv', 'w+', newline='')