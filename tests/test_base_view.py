def test_root(client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert resp.data == b'Hello World'
